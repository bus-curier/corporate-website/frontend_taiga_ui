import {ChangeDetectionStrategy, Component} from '@angular/core'

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicesComponent {
  constructor() {}

  imgLoad() {
    // alert(1)
  }
}

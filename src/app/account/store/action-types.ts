export enum ActionTypes {
  GET_BALANCE = '[Account] Get balance',
  GET_BALANCE_SUCCESS = '[Account] Get balance success',
  GET_BALANCE_FAILURE = '[Account] Get balance failure',

  //TODO: need to remove
  GET_USER_PROFILE = '[Account] Get user profile',
  GET_USER_PROFILE_SUCCESS = '[Account] Get user profile success',
  GET_USER_PROFILE_FAILURE = '[Account] Get user profile',
}
